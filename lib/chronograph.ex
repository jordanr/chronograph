defmodule Chronograph do
  @moduledoc """
  Documentation for `Chronograph`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Chronograph.hello()
      :world

  """
  def hello do
    :world
  end
end
