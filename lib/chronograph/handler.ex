defmodule Chronograph.Handler do
  use Speedmaster.Handler
  require Logger
  import Speedmaster.Handler
  defp echo(capture, _query) do
    request = Map.get(capture, "anything", "Nothing in the url")
    Logger.info("received a request with request = #{request}")
    respond_gmi("You requested " <> request)
  end

  def handler(request) do
    match("/:anything", &echo/2)
    |> applymatch(request)
  end
end
