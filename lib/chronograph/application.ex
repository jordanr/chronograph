defmodule Chronograph.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Speedmaster,
       [handler: Chronograph.Handler,
        certfile: "./priv/speedmaster/certs/selfsigned/certificate.pem",
        keyfile: "./priv/speedmaster/certs/selfsigned/privatekey.key"]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Chronograph.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
